package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"
)

const (
	pathOutput        = "retire.json"
	flagPythonVersion = "python-version"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   flagPythonVersion,
			EnvVar: "DS_PYTHON_VERSION",
			Usage:  "Set the version of Python to be used to install npm packages.",
		},
	}
}

func pathExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func setPythonVersionCommand(c *cli.Context) *exec.Cmd {
	pythonVersion := c.String(flagPythonVersion)
	if strings.HasPrefix(pythonVersion, "2") {
		fmt.Println("Using python 2.7")
		return exec.Command("npm", "config", "set", "python", "/usr/bin/python2.7")

	}

	fmt.Println("Using python 3")
	return exec.Command("npm", "config", "set", "python", "/usr/bin/python3")

}

func installDependencies(path string) error {
	fmt.Println(aurora.Green("Installing dependencies..."))

	cmd := exec.Command("npm", "install")

	if pathExists(path + "/yarn.lock") {
		cmd = exec.Command("yarn", "install", "--ignore-engines")
	}

	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {

	// Install node_modules needed by retire.js, if none are present
	if !pathExists(path + "/node_modules") {
		if stdoutStderr, err := setPythonVersionCommand(c).CombinedOutput(); err != nil {
			fmt.Println(string(stdoutStderr))
			fmt.Println(aurora.Red("Could not set Python version"))
			return nil, err
		}

		installDependencies(path)
	} else {
		fmt.Println(aurora.Green("node_modules detected, skipping installation."))
	}

	cmd := exec.Command("retire", "--outputformat", "jsonsimple", "--outputpath", pathOutput, "--exitwith", "0")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathOutput))
}
