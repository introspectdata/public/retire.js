module gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2

require (
	github.com/logrusorgru/aurora v0.0.0-20181002194514-a7b3b318ed4e
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.1.6
)
