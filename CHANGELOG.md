# Retire.js analyzer changelog

## v2.2.1

- Fix "engine 'node' is incompatible with this module" error

## v2.2.0

- Add Python 3 to the Docker image (!14)
- Add `DS_PYTHON_VERSION` variable, to be set to `2` to switch to Python 2 (!14)

## v2.1.0

- Add support for git dependencies

## v2.0.3

- Update common to 2.1.6

## v2.0.2
- Install node_modules via npm if none are detected
- Install node_modules via yarn if none are detected

## v2.0.1
- Bump RetireJS to 2.0.2
- Update file identifiers for child dependencies to point to parent file location

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Bump RetireJs to 1.6.2

## v1.1.1
- Fix empty `location.file` field, use `package.json` as a default

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
