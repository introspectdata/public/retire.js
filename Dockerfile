FROM node:11-alpine

ARG RETIRE_JS_VERSION
ENV RETIRE_JS_VERSION ${RETIRE_JS_VERSION:-2.0.2}

# Version of Python, defaults to Python 3
ARG DS_PYTHON_VERSION
ENV DS_PYTHON_VERSION ${DS_PYTHON_VERSION:-3}

# install git for project with git-sourced dependencies
RUN apk add --no-cache git  build-base python python3

RUN pip3 install --upgrade pip setuptools && \
    pip install --upgrade pip setuptools

# temporary workaround for
# https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011
RUN npm config set unsafe-perm true

RUN npm install -g retire@$RETIRE_JS_VERSION yarn

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
